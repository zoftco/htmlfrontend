const gulp = require('gulp');
const del = require('del');
const cssnano = require("cssnano");
const pump = require('pump');
const uglify = require('gulp-uglify');
const rename = require('gulp-rename');
const sass = require('gulp-sass');
const include = require('gulp-include');
const postcss = require('gulp-postcss');
const postcssCopy = require('postcss-copy');
const postcssImport = require("postcss-import");
const autoprefixer = require('autoprefixer');
const sourcemaps = require('gulp-sourcemaps');
const render = require('gulp-nunjucks-render');
const browserSync = require('browser-sync').create();

gulp.task('serve', function() {
    browserSync.init({
        port: 9000,
        open: false,
        notify: false,
        server: {
            baseDir: "./public"
        }
    });
});

gulp.task('sass', function (callback) {
    // Default postcss processors
    const processors = [
        postcssImport(),
        postcssCopy({
            dest: 'css',
            basePath: ['.'],
            template: function (meta) {
                return `${meta.ext}/${meta.name}.${meta.hash}.${meta.ext}${meta.query}`
            }
        }),
    ];

    // Processors that are just enabled in production
    // use `make` as a shortcut to call gulp with the production environment
    if (process.env.NODE_ENV === 'production') {
        processors.push(autoprefixer({
            browsers: [
                'last 3 versions',
                '> 1%',
                'ie 8',
            ]
        }));

        processors.push(cssnano({
            mergeRules: true,
            reduceIdents: false,
            discardUnused: true,
            orderedValues: true,
            styleCache: true,
            reduceTransforms: true,
            minifySelectors: true,
            zindex: false,
        }))
    }

    const tasks = [
        gulp.src(['sass/*.scss']),
        sourcemaps.init(),
        sass().on('error', sass.logError),
        sourcemaps.write(),
        postcss(processors),
    ];

    // Rename it to .min when on production
    if (process.env.NODE_ENV === 'production') {
        tasks.push(rename({'suffix': '.min'}));
    }

    tasks.push(gulp.dest('public/css'));
    tasks.push(browserSync.stream());

    pump(tasks, callback);
});

gulp.task('scripts', function (callback) {
    const tasks = [
        gulp.src('scripts/*.js'),
        include({extensions: 'js'}).on('error', console.log),
    ];

    // Just minify and rename the scripts when on production mode
    if (process.env.NODE_ENV === 'production') {
        tasks.push(uglify());
        tasks.push(rename({'suffix': '.min'}));
    }

    tasks.push(gulp.dest('public/js'));

    // pump runs tasks together, but it safely stop if one them closes
    pump(tasks, callback);
});

gulp.task('html', function() {
  return gulp.src('templates/*.html')
    .pipe(render({path: ['templates']}))
    .pipe(gulp.dest('public'))
});

gulp.task('watch', function () {
    gulp.watch(['templates/**/*'], ['html']);
    gulp.watch(['sass/*.scss', 'sass/**/*.scss'], ['sass']);
    gulp.watch(['scripts/*.js', 'scripts/**/*.js'], ['scripts']);
    gulp.watch(['public/*.html', 'public/**/*.html']).on('change', browserSync.reload);
    gulp.watch(['public/*.js', 'public/**/*.js']).on('change', browserSync.reload);
});

gulp.task('clean', function () {
    return del(['css/*.min.css', 'js/*.min.js'])
});

gulp.task('dist', ['sass', 'scripts']);
gulp.task('default', ['sass', 'scripts', 'watch', 'serve']);
